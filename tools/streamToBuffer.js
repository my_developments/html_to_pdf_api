function streamToBuffer(stream, cb) {
  const chunks = [];
  
  stream.on('data', (chunk) => {
    chunks.push(chunk);
  });
  stream.on('end', () => {
    return cb(null, Buffer.concat(chunks));
  });
  stream.on('error', (e) => {
    return cb(e);
  });
}

module.exports = streamToBuffer;