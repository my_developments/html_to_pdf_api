const express = require('express');
const pdf = require('html-pdf');
const streamToBuffer = require('../tools/streamToBuffer');

const router = express.Router();

router.post('/htmlToPdf', (req, res) => {
  try {
    const html = req.body.html; // It's equals to fs.readFileSync('./temp/temp.html', 'utf-8');
    // const html = fs.readFileSync('./temp/temp.html', 'utf-8');
    if (html === undefined || html === null) {
      return res.status(400).json({
        "status": "400",
        "message": "Archivo de entrada indefinido",
        "payload": {
          "error": "Archivo de entrada indefinido o vacío"
        }
      });
    }

    new Promise((resolve, reject) => {
      pdf.create(html({})).toStream((err, stream) => {
        if (err)
          return reject(err);
        return resolve(stream);
      })
    })
    .then((stream) => {
      streamToBuffer(stream, (error, buffer) => {
        if (error) {
          console.error("Fails throughout stream conversion | ", error);
          return res.status(400).json({
            "status": "400",
            "message": "Error al convertir el stream a buffer",
            "payload": {
              "error": error
            }
          });
        }
        
        console.info("File converted successfully | ", buffer);

        // fs.writeFile("./temp/temp.pdf", buffer, (err) => {
        //   if (err)
        //     console.error(err);
        //   console.log("File created successfully");
        // })

        return res.status(200).json({
          "status": "200",
          "message": "Éxito al convertir la entrada a PDF",
          "payload": {
              "buffer": buffer,
          }
        });
      });
    })
  } catch (error) {
    console.error("Fails throughout file conversion | ", error);
    return res.status(400).json({
        "status": "400",
        "message": "Error al convertir la entrada a PDF",
        "payload": {
          "error": error
        }
      });
  }
});

module.exports = router;